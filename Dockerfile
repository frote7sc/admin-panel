# build the Vue application
FROM node:lts-alpine as dist
WORKDIR /opt/project
COPY ./ ./
RUN npm i
ENV NODE_ENV=production
RUN npm run build

# build the nginx image
FROM nginx:1.23.3
EXPOSE 80
COPY --from=dist /opt/project/dist /usr/share/nginx/html
COPY ./nginx.conf /etc/nginx/nginx.conf